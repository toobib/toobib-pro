import frappe

from toobibpro.demo.demo import setup_wizard


def before_tests():
	if not frappe.db.exists("User", "lucile@toobib.org"):
		setup_wizard()
