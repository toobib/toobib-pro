import frappe
from frappe.utils.data import add_to_date, cstr, getdate

from toobibpro.accounting.utils import FiscalYearError, get_fiscal_year


def boot_session(bootinfo):
	"""boot session - send website info if guest"""

	if frappe.session.user != "Guest" and bootinfo.get("home_page") != "setup-wizard":
		bootinfo.practitioner = frappe.db.get_value(
			"Professional Information Card", dict(user=frappe.session.user), "name"
		)

		try:
			bootinfo.fiscal_year = get_fiscal_year(date=getdate(), practitioner=bootinfo.practitioner)
		except FiscalYearError:
			today = getdate()
			assert today

			start = today.replace(month=1, day=1)
			end = add_to_date(start, years=1, days=-1)
			start_year = cstr(start.year)
			end_year = cstr(end.year)

			# Create a new fiscal year
			fy = frappe.new_doc("Fiscal Year")
			fy.year_start_date = start
			fy.year_end_date = end

			if start_year == end_year:
				fy.year = start_year
			else:
				fy.year = start_year + "-" + end_year
			fy.auto_created = 1

			fy.insert(ignore_permissions=True)
		except Exception:
			frappe.log_error()
