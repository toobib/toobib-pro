# Copyright (c) 2019, Dokos and Contributors
# See license.txt

import click
import frappe
from frappe.commands import get_site, pass_context


@click.command("make-demo")
@click.option("--reinstall", is_flag=True, help="Reinstall site")
@click.option("--resume", is_flag=True, help="Resume generation")
@click.option("--days", default=365, help="Run the demo for so many days. Default 365")
@click.option("--admin-password", help="Administrator Password for reinstalled site")
@click.option("--db-root-username", help="Root username for DB")
@click.option("--db-root-password", help="Root password for DB")
@pass_context
def make_demo(
	context,
	reinstall=False,
	resume=False,
	days=365,
	admin_password=None,
	db_root_username=None,
	db_root_password=None,
):
	"Reinstall site and setup a demo for ToobibPro"
	from frappe.commands.site import _reinstall

	site = get_site(context)
	if not site:
		raise click.UsageError("No site selected")

	if reinstall == resume:
		raise click.UsageError("Use either --reinstall or --resume, not both or none")

	if reinstall:
		_reinstall(
			site=site,
			admin_password=admin_password,
			db_root_username=db_root_username,
			db_root_password=db_root_password,
			yes=True,
		)

	with frappe.init_site(site):
		from toobibpro.demo import demo

		frappe.connect()
		if reinstall:
			demo.make(days=days)
		else:
			demo.resume(days=days)


commands = [make_demo]
