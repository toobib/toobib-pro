# Copyright (c) 2019, DOKOS and Contributors
# See license.txt


import frappe
from frappe import _
from frappe.utils import getdate, today


def update_patient_birthday():
	"""This function is called by a daily scheduled task to update the patient age and spouse age of all patients."""
	patients = frappe.get_all(
		"Patient Record",
		fields=[
			"name",
			"patient_date_of_birth",
			"patient_age",
			"spouse_date_of_birth",
			"spouse_age",
			"owner",
		],
	)

	for patient in patients:
		frappe.local.lang = frappe.db.get_value("User", patient.owner, "language") or "fr"
		_set_patient_age(patient)
		_set_patient_spouse_age(patient)


def set_patient_and_spouse_age_in_document(doc):
	"""This function can be called on the validate event of a Patient Record to set the patient age and spouse age."""
	frappe.local.lang = frappe.db.get_value("User", doc.owner, "language") or "fr"

	patient_age = _convert_dob_to_age(doc.patient_date_of_birth, doc.patient_age)
	if patient_age:
		doc.patient_age = patient_age

	spouse_age = _convert_dob_to_age(doc.spouse_date_of_birth, doc.spouse_age)
	if spouse_age:
		doc.spouse_age = spouse_age


def _set_patient_age(patient: frappe._dict):
	if patient_age := _convert_dob_to_age(patient.patient_date_of_birth, patient.patient_age):
		frappe.db.set_value("Patient Record", patient.name, "patient_age", patient_age, modified=False)


def _set_patient_spouse_age(patient: frappe._dict):
	if spouse_age := _convert_dob_to_age(patient.spouse_date_of_birth, patient.spouse_age):
		frappe.db.set_value("Patient Record", patient.name, "spouse_age", spouse_age, modified=False)


def _convert_dob_to_age(dob: str, age: str):
	if dob:
		new_age = _calculate_age(getdate(dob))
		if not age or _extract_int_from_string(age) != int(new_age):
			return _("{0} years old").format(new_age)


def _calculate_age(birthdate):
	nowday = getdate(today())
	return (
		nowday.year - birthdate.year - ((nowday.month, nowday.day) < (birthdate.month, birthdate.day))
	)


def _extract_int_from_string(string: str):
	return int("".join([char for char in string if char.isdigit()]))
