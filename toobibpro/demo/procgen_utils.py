# Copyright (c) 2023, Dokos SAS and Contributors

# Procgen stands for procedural generation,
# the process of automatically creating random content with some kind of algorithm or ruleset.

import json
import random
import re
from datetime import timedelta
from typing import TYPE_CHECKING, Callable

import frappe
import frappe.utils

if TYPE_CHECKING:
	from frappe.model.document import Document


def j(name: str):
	path = frappe.get_app_path("toobibpro", "demo", "data", name)

	with open(path) as f:
		if name.endswith(".json"):
			return json.load(f)
		elif name.endswith(".jsonc"):
			contents = f.read()
			# remove C/C++ style comments
			contents = re.sub(r"/\*.*?\*/", "", contents, flags=re.DOTALL)
			contents = re.sub(r"//.*?\n", "", contents)
			return json.loads(contents)
		elif name.endswith(".txt"):
			return list(map(str.strip, filter(None, f.readlines())))


specs: list["Document"] = j("specs.jsonc")
first_names: list[str] = j("first_names.txt")
last_names: list[str] = j("last_names.txt")
fixtures: list["Document"] = j("fixtures.jsonc")
origins: list[str] = j("origins.txt")
professions: list[str] = j("professions.txt")
urinary_infection_details: list[str] = j("urinary_infection_details.txt")
high_blood_pressure_details: list[str] = j("high_blood_pressure_details.txt")
ophtalmology_details: list[str] = j("ophtalmology_details.txt")
migraine_details: list[str] = j("migraine_details.txt")
diabetes_cholesterol_cholestasis_details: list[str] = j(
	"diabetes_cholesterol_cholestasis_details.txt"
)
cerebrovascular_accident_aneurysm_phlebitis_details: list[str] = j(
	"cerebrovascular_accident_aneurysm_phlebitis_details.txt"
)
hormonal_dependent_cancer_details: list[str] = j("hormonal_dependent_cancer_details.txt")
long_term_disease: list[str] = j("long_term_disease.txt")
contraception: list[str] = j("contraception.txt")
assistant_medical_procreation_details: list[str] = j("assistant_medical_procreation_details.txt")
additional_comments: list[str] = j("additional_comments.txt")
spouse_profession: list[str] = j("spouse_profession.txt")


def insert_fixtures():
	for fixture in fixtures:
		if not fixture:
			continue
		doc = frappe.get_doc(fixture)
		try:
			doc.insert(ignore_permissions=True)
		except frappe.DuplicateEntryError:
			pass
			# frappe.delete_doc(doc.doctype, doc.name, ignore_permissions=True)
			# doc.insert(ignore_permissions=True)


def _synthetize(doctype: str):
	spec = {
		df.fieldname: "$random" if df.reqd else "$random?"
		for df in frappe.get_meta(doctype).fields
		if not df.read_only and not df.hidden
	}
	spec["doctype"] = doctype
	return spec


def _get_spec(spec_name: str):
	if not spec_name:
		raise RuntimeError("Spec name cannot be empty")

	if spec_name.startswith("$synthetize:"):
		doctype = spec_name.split(":", 1)[1]
		spec = _synthetize(doctype)
		return spec

	"""Get the spec for the given doctype"""
	for spec in specs:
		if spec.get("$fragment") == spec_name:
			return spec
		elif spec.get("$fragment"):
			continue  # skip fragments
		elif spec.get("$fixture"):
			continue  # skip fixtures
		elif spec.get("$synthetize"):
			return _synthetize(spec_name)
		elif spec.get("doctype") == spec_name:
			if any(v == "" for v in spec.values()):
				raise RuntimeError(f"Spec for {spec_name!r} is incomplete")
			return spec

	raise RuntimeError(f"Spec for {spec_name!r} not found")


def generate_table(
	table_spec: dict | str | int,
	child_dt: str | None = None,
	parent: "frappe.Document | None" = None,
):
	if isinstance(table_spec, int):
		table_spec = {"_count": table_spec}
	elif isinstance(table_spec, str):
		if " x " in table_spec:
			count, spec_name = table_spec.split(" x ")
			table_spec = {"_count": int(count), "spec": spec_name}
		else:
			table_spec = {"_count": table_spec}
	elif isinstance(table_spec, list):
		return table_spec
	elif isinstance(table_spec, dict):
		pass

	count = _eval(table_spec.get("_count", 0))

	if count == 0:
		raise RuntimeError("Cannot generate a table with 0 rows")

	if spec := table_spec.get("spec"):
		spec = _get_spec(spec)
		child_dt = spec.get("doctype")
		out = []
		for _ in range(count):
			out.append(generate_doc(child_dt, spec=spec, _parent=parent))
		return out

	if child_dt is None:
		raise RuntimeError("Cannot generate a table without a child DocType")

	out = []
	for _ in range(count):
		out.append(generate_doc(child_dt, _parent=parent, **table_spec))

	return out


def get_random_docname(dt: str):
	"""Get a random document of the given type"""
	n = frappe.db.count(dt)
	if n == 0:
		raise RuntimeError(f"There are no documents of DocType {dt!r}")
	offset = random.randint(0, n - 1)
	return frappe.get_all(dt, limit_page_length=1, limit_start=offset)[0].name


def _eval(x: str | int):
	if isinstance(x, int):
		return x
	elif isinstance(x, str):
		if x.startswith("$randint("):
			_, start, end = x.split(",")
			return random.randint(int(start), int(end))
		else:
			raise RuntimeError(f"Cannot evaluate {x!r}")
	else:
		raise RuntimeError(f"Cannot evaluate {x!r}")


previously_generated = {
	"first_name": "test",
	"last_name": "test",
}


def generate_data(df: frappe._dict, spec: list | str, parent: "frappe.Document | None" = None):
	specs = [spec] if isinstance(spec, str) else spec
	match specs:
		case ["$random"]:
			dt = df.options if df else None
			if dt:
				docname = get_random_docname(dt)
			else:
				return generate_data(df, None, parent=parent)
			if not docname:
				raise RuntimeError(f"Cannot find random {dt!r}")
			return docname
		case ["$random?"]:
			if df and df.options:
				try:
					return get_random_docname(df.options)
				except RuntimeError:
					return None
			else:
				return generate_data(df, None, parent=parent)
		case ["$random:doc", dt, filters]:
			docname = frappe.db.get_value(dt, filters, "name")
			if not docname:
				raise RuntimeError(f"Cannot find random {dt!r} with filters {filters!r}")
			return docname
		case ["$random:bool"]:
			return random.random() < 0.5
		case ["$random:bool", proba]:
			return random.random() < float(proba)
		case ["$random:int", start, end]:
			return random.randint(int(start), int(end))
		case ["$random:float", start, end]:
			return random.uniform(start, end)
		case ["$random:choices", *choices] | ["$in", *choices]:
			return random.choice(choices)
		case ["$random:date", start, end]:
			delta = frappe.utils.getdate(end) - frappe.utils.getdate(start)
			delta = random.randrange(delta.days * 24 * 60 * 60)
			return frappe.utils.getdate(start) + timedelta(seconds=delta)
		case ["$random:today:time"]:
			hour = random.randint(9, 18)
			minute = random.choice([0, 15, 30, 45])
			date = frappe.utils.nowdate() + f" {hour:02}:{minute:02}:00"
			return date
		case ["$random:text"]:
			return " ".join([random.choice(["lorem", "ipsum", "dolor", "sit", "amet"]) for _ in range(5)])
		case ["$first_name"]:
			x = previously_generated["first_name"] = random.choice(first_names)
			return x
		case ["$last_name"]:
			x = previously_generated["last_name"] = random.choice(last_names)
			return x
		case ["$origin"]:
			name = random.choice(origins)
			return generate_data(
				None,
				[
					"$get_or_generate",
					["Origin", {"origin": name}],
					{"spec_name": "$synthetize:Origin", "origin": name},
				],
			)
		case ["$profession"]:
			name = random.choice(professions)
			return generate_data(
				None,
				[
					"$get_or_generate",
					["Profession", {"profession_name": name}],
					{"spec_name": "$synthetize:Profession", "profession_name": name},
				],
			)
		case ["$spouse_profession"]:
			name = random.choice(spouse_profession)
			return generate_data(
				None,
				[
					"$get_or_generate",
					["Profession", {"profession_name": name}],
					{"spec_name": "$synthetize:Profession", "profession_name": name},
				],
			)
		case ["$contraception"]:
			name = random.choice(contraception)
			return generate_data(
				None,
				[
					"$get_or_generate",
					["Contraception", {"contraception": name}],
					{"spec_name": "$synthetize:Contraception", "contraception": name},
				],
			)
		case ["$random_file_value", filename]:
			# urinary_infection_details
			return random.choice(eval(filename))
		case ["$high_blood_pressure_details"]:
			return random.choice(high_blood_pressure_details)
		case ["$ophtalmology_details"]:
			return random.choice(ophtalmology_details)
		case ["$migraine_details"]:
			return random.choice(migraine_details)
		case ["$diabetes_cholesterol_cholestasis_details"]:
			return random.choice(diabetes_cholesterol_cholestasis_details)
		case ["$cerebrovascular_accident_aneurysm_phlebitis_details"]:
			return random.choice(cerebrovascular_accident_aneurysm_phlebitis_details)
		case ["$hormonal_dependent_cancer_details"]:
			return random.choice(hormonal_dependent_cancer_details)
		case ["$long_term_disease"]:
			return random.choice(long_term_disease)
		case ["$email"]:
			local_part = f"{previously_generated['first_name']}.{previously_generated['last_name']}"
			local_part = local_part.encode("ascii", "ignore").decode("ascii")
			local_part = frappe.scrub(local_part)
			return f"{local_part}@example.com"
		case ["$today"]:
			return frappe.utils.nowdate()
		case ["$table", count, child_spec]:
			if isinstance(child_spec, str):
				return generate_table({"_count": count, "spec": child_spec}, parent=parent)

			count = generate_data(None, count, parent=parent)
			child_dt = df.options if df else child_spec["doctype"]
			if not child_dt or child_spec.get("doctype", child_dt) != child_dt:
				raise RuntimeError(f"Cannot generate table for {child_dt!r}")
			return [
				generate_doc(child_dt, spec=child_spec, __do_not_insert=True, _parent=parent)
				for _ in range(count)
			]
		case ["$cases", *cases]:
			return {"$cases": cases}
		case ["$fetch_from", s]:
			src, fieldname = s.split(".")
			return {"$fetch_from": (src, fieldname)}
		case ["$fetch_from", src, fieldname]:
			return {"$fetch_from": (src, fieldname)}
		case ["$eval", txt]:
			return {"$eval": txt}
		case ["$get_or_generate", get_params, generate_params]:
			return {"$get_or_generate": (get_params, generate_params)}
		case ["$reqd"]:
			return None

	if isinstance(spec, (str, int, float, bool)):
		return spec

	if not df:
		return spec

	if df.fieldtype in ("Small Text", "Text", "Text Editor", "Data", "Code"):
		return " ".join([random.choice(["lorem", "ipsum", "dolor", "sit", "amet"]) for _ in range(5)])

	print(f"generate_data({df.fieldtype!r}, {spec!r}, {parent!r})")

	if df.fieldtype == "Table":
		return generate_table(spec, df.options, parent=parent)
	elif df.fieldtype == "Link":
		return get_random_docname(df.options)
	elif df.fieldtype == "Select":
		return random.choice(df.options.splitlines())
	elif df.fieldtype == "Int":
		return random.randint(0, 100)
	elif df.fieldtype == "Float":
		return random.random() * 100
	elif df.fieldtype == "Check":
		return random.choice([True, False])
	elif df.fieldtype == "Date" or df.fieldname == "date":
		return frappe.utils.nowdate()
	elif df.fieldtype == "Datetime":
		return frappe.utils.now_datetime()
	elif df.fieldtype == "Time":
		return frappe.utils.nowtime()
	elif df.fieldtype == "Color":
		return "#%06x" % random.randint(0, 0xFFFFFF)
	elif df.fieldtype == "Currency":
		return random.randint(0, 100)
	elif df.fieldtype == "Percent":
		return random.randint(0, 100)

	raise RuntimeError(f"Cannot generate data for {df.fieldtype!r}")


def get_fragment(fragment: str, for_spec: dict):
	for spec in specs:
		if fragment_name := spec.get("$fragment"):
			if fragment_name == fragment:
				return spec


def generate_doc(spec_name: str, spec=None, _parent: "frappe.Document | None" = None, **kwargs):
	assert frappe.session.user == "Administrator", "Only the Administrator can generate test data"

	"""Create a new document with the given type and attributes"""
	spec = spec or _get_spec(spec_name)

	if fragments := spec.get("$extends"):
		spec = {**spec}
		fragments = list(fragments) if isinstance(fragments, list) else [fragments]
		while fragments:
			fragment = fragments.pop(0)
			fragment = get_fragment(fragment, spec) or _get_spec(fragment)
			if fragment:
				if fragment.get("$extends"):
					ext = fragment["$extends"]
					ext = ext if isinstance(ext, list) else [ext]
					fragments = ext + fragments
				spec = {**fragment, "$fragment": None, **spec}

	dt = spec.get("doctype", spec_name)
	meta = frappe.get_meta(dt)

	instantiated = {
		"doctype": dt,
	}
	for k, v in spec.items():
		if k.startswith("_") or k.startswith("$") or k == "doctype" or v == "$void":
			continue

		if k in kwargs:
			instantiated[k] = kwargs[k]
			continue

		if k == "name":
			continue

		if df := meta.get_field(k):
			instantiated[k] = generate_data(df, v, parent=instantiated)
			continue

		raise RuntimeError(f"Field {k!r} not found in DocType {dt!r}")

	# Evaluate $cases, etc.
	instantiated = frappe._dict(instantiated)
	ctx = {"doc": instantiated, "parent": _parent and frappe._dict(_parent) or None}

	for k, v in instantiated.items():
		match v:
			case {"$cases": cases}:
				for cond, val in cases:
					if frappe.safe_eval(cond, None, ctx):
						df = meta.get_field(k)
						instantiated[k] = generate_data(df, val, parent=instantiated)
						break
			case {"$fetch_from": (src, fieldname)}:
				doc_type = meta.get_field(src).options
				doc_name = instantiated[src]
				instantiated[k] = frappe.db.get_value(doc_type, doc_name, fieldname)
				assert instantiated[k], f"Could not fetch {fieldname!r} from {doc_type!r} {doc_name!r}"
			case {"$eval": code}:
				instantiated[k] = do_eval(ctx, code)
			case {"$get_or_generate": (get_params, generate_params)}:
				if docname := get_or_generate(
					lambda: do_object_eval(ctx, get_params),
					lambda: do_object_eval(ctx, generate_params),
				):
					instantiated[k] = docname
				else:
					raise RuntimeError(f"Could not get or generate {k!r}")

	doc = frappe.get_doc(instantiated)
	for tbl in meta.get_table_fields():
		if tbl.fieldname in instantiated:
			doc.set(tbl.fieldname, [])
			for child in instantiated[tbl.fieldname]:
				doc.append(tbl.fieldname, child)

	return doc


def do_eval(ctx, code):
	assert frappe.session.user == "Administrator", "Only the Administrator EVAL data"
	return eval(code, None, ctx)


def do_object_eval(ctx, obj):
	assert frappe.session.user == "Administrator", "Only the Administrator EVAL data"
	if isinstance(obj, str) and obj.startswith("$raise:"):
		raise RuntimeError(obj[7:])
	elif isinstance(obj, str) and obj.startswith("$eval:"):
		return eval(obj[6:], None, ctx)
	elif isinstance(obj, dict):
		return {k: do_object_eval(ctx, v) for k, v in obj.items()}
	elif isinstance(obj, list):
		return [do_object_eval(ctx, v) for v in obj]
	else:
		return obj


def get_or_generate(
	get_value_params: list | dict | Callable, generate_params: list | dict | Callable
) -> str:
	"""Get a document from the database or generate a new one if it doesn't exist"""

	def _call(f, args):
		if isinstance(args, list):
			return f(*args)
		elif isinstance(args, dict):
			return f(**args)
		elif callable(args):
			return _call(f, args())
		else:
			raise RuntimeError(f"Invalid arguments: {args!r}")

	if docname := _call(frappe.db.get_value, get_value_params):
		return docname
	else:
		doc = _call(generate_doc, generate_params)
		doc.insert()
		return doc.name
