# Copyright (c) 2020, DOKOS and contributors
# For license information, please see license.txt


import frappe
from frappe.model.document import Document

keydict = {"currency": "currency"}


class ToobibProSettings(Document):
	def on_update(self):
		for key in keydict:
			frappe.db.set_default(key, self.get(keydict[key], ""))

		if self.currency:
			frappe.db.set_value("Currency", self.currency, "enabled", 1)
