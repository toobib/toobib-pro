// Copyright (c) 2023, DOKOS and contributors
// For license information, please see license.txt

frappe.provide("toobibpro")
frappe.provide("toobibpro.patient_record");
frappe.provide("toobibpro.patient");

frappe.ui.form.on("Patient Record", {
	onload: function(frm) {
		frm.set_query("website_user", function() {
			return {
				query: "toobibpro.patients.doctype.patient_record.patient_record.get_users_for_website"
			}
		});
		setup_chart(frm);
	},
	refresh: function(frm) {
		frappe.dynamic_link = {
			doc: frm.doc,
			fieldname: 'name',
			doctype: 'Patient Record'
		};

		if (frm.doc.__islocal) {
			hide_field(['address_html']);
			frappe.contacts.clear_address_and_contact(frm);
		} else {
			unhide_field(['address_html']);
			frappe.contacts.render_address_and_contact(frm);

			frappe.db.get_value("Professional Information Card", {user: frappe.session.user}, "disable_accounting", (r) => {
				if (!r.disable_accounting) {
					toobibpro.patient_record.set_dashboard_indicators(frm);
				}
			})
		}
		setup_chart(frm);
		add_folder_print_btn(frm);

		if (frm.doc.patient_date_of_birth) {
			calculate_age(frm, "patient_date_of_birth", "patient_age");
		}
		if (frm.doc.spouse_date_of_birth) {
			calculate_age(frm, "spouse_date_of_birth", "spouse_age");
		}

		frm.trigger("check_phone_number_format");

		new toobibpro.patient_record_timeline({
			parent: frm.get_field("lifeline").$wrapper,
			frm: frm,
			section: $(frm.get_field("lifeline_section").wrapper)
		})

		frm.trigger("setup_action_buttons");
		frm.trigger("show_linked_attachments");

	},
	check_phone_number_format: function(frm) {
		if (frm.doc.mobile_no) {
			const reg = /^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/
			if (!frm.doc.mobile_no.match(reg)) {
				frappe.msgprint(
					"Exemples de n° valides (France uniquement) : +33612345678, 0033612345678, 0612345678, 06 12 34 56 78",
					__("The mobile n° format is incorrect")
				);
			}
		}
	},
	invite_as_user: function(frm) {
		frm.save();
		let d = new frappe.ui.Dialog({
			'title': __('Create a New Website User ?'),
			fields: [{
					fieldtype: "HTML",
					options: __("Are you certain you want to create a website user for this patient ?")
				},
				{
					fieldname: 'ok_button',
					fieldtype: 'Button',
					label: __("Yes")
				},
			]
		});
		d.show();
		d.fields_dict.ok_button.input.onclick = function() {
			d.hide();
			return frappe.call({
				method: "toobibpro.patients.doctype.patient_record.patient_record.invite_user",
				args: {
					patient: frm.doc.name
				},
				callback: function(r) {
					if (r.message){
							frm.set_value("website_user", r.message);
							frm.save();
					} else {
						frappe.msgprint(__("Something went wrong during the user creation.<br>Please check with the support team."))
					}

				}
			});
		};
	},
	weight: function(frm) {
		calculate_bmi(frm);
		if (frm.doc.weight) {
			frappe.show_alert({
				message: __("Don't forget to save your patient record before leaving"),
				indicator: 'orange'
			});
			frappe.call({
				method: "toobibpro.patients.doctype.patient_record.patient_record.update_weight_tracking",
				args: {
					doc: frm.doc.name,
					weight: frm.doc.weight
				},
				callback: function(r) {
					if (r.message == 'Success') {
						frappe.show_alert({
							message: __("Weight Updated"),
							indicator: 'green'
						});
						setup_chart(frm);
					}
				}
			})
		}
	},
	height: function(frm) {
		calculate_bmi(frm);
	},
	patient_date_of_birth: function(frm) {
		calculate_age(frm, "patient_date_of_birth", "patient_age");
	},
	spouse_date_of_birth: function(frm) {
		calculate_age(frm, "spouse_date_of_birth", "spouse_age");
	},
	pregnancies_report: function(frm) {
		frappe.set_route('pregnancies', frm.doc.name);
	},
	disable_user: function(frm) {
		if (frm.doc.website_user) {
			frappe.xcall("toobibpro.patients.doctype.patient_record.patient_record.disable_user", {user: frm.doc.website_user, status: !frm.doc.disable_user})
			.then(e => {
				frappe.show_alert({ message: __("Website user {0}", [frm.doc.disable_user ? __("disabled") : __("enabled")]), indicator: 'green' })
			})
			.catch(() => {
				frappe.show_alert({ message: __("Error while disabling the website user. Please contact the support."), indicator: 'orange' })
			})
		}

	},
	setup_action_buttons: function(frm) {
		frappe.call({
			method: "toobibpro.patients.doctype.patient_record.patient_record.get_permitted_action_documents",
		}).then(r => {
			r.message.map(doc => {
				frm.page.add_action_item(
					__("Create a {0}", [__(doc)]),
					() => {
						return frappe.new_doc(doc, {
							"patient_record": frm.doc.name
						})
					}
				)
			})

			frm.page.btn_primary.addClass("hide");
		})
	},
	show_linked_attachments: function(frm) {
		frappe.call({
			method: "toobibpro.patients.doctype.patient_record.patient_record.get_linked_attachments",
			args: {
				patient_record: frm.doc.name
			}
		}).then(r => {
			new DocumentGrid({
				data: r.message,
				wrapper: frm.get_field("documents_html").$wrapper
			})
		})
	}
});

frappe.ui.form.on('Patient Sports', {
	sport: function(frm, cdt, cdn) {
		const row = locals[cdt][cdn];
		frappe.db.get_value('Sport', {name: row.sport}, 'recommendations', (r) => {
				frappe.model.set_value(cdt, cdn, "recommendations", r.recommendations);
		});
	}
});


const calculate_age = (frm, source, target) => {
	const today = new Date();
	const birthDate = new Date(frm.doc[source]);
	if (today < birthDate) {
		frappe.msgprint(__('Please select a valid Date'));
		frappe.model.set_value(frm.doctype, frm.docname, source, null)
	} else {
		let age_yr = today.getFullYear() - birthDate.getFullYear();
		const today_m = today.getMonth() + 1 //Month jan = 0
		const birth_m = birthDate.getMonth() + 1 //Month jan = 0
		const m = today_m - birth_m;

		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age_yr--;
		}

		let age_str = null
		if (age_yr > 0)
			age_str = __('{0} years old', [age_yr]);

		frappe.model.set_value(frm.doctype, frm.docname, target, age_str);
	}
	frm.refresh_field(target);
};

const calculate_bmi = (frm) => {
	const weight = frm.doc.weight;
	const height = frm.doc.height;
	const bmi = height ? Math.round(weight / Math.pow(height, 2)) : 0;
	frappe.model.set_value(frm.doctype, frm.docname, "body_mass_index", bmi)
};

const setup_chart = (frm) => {
	frappe.call({
		method: "toobibpro.patients.doctype.patient_record.patient_record.get_patient_weight_data",
		args: {
			patient_record: frm.doc.name
		},
		callback: function(r) {
			if (r.message && r.message[0].datasets[0].values.length !=0) {
				const data = r.message[0];

				if (data.datasets[0].values.length > 1) {
					const $wrap = $('div[data-fieldname=weight_curve]').get(0);
					new frappe.Chart($wrap, {
						title: __("Patient Weight"),
						data: data,
						type: 'line',
						lineOptions: {
							regionFill: 1
						},
						height: 240,
						format_tooltip_y: d => d + ' Kg',
						colors: ['#ffa00a'],
					});
				} else {
					const empty_text = __("Not enough data for a chart yet")
					$('div[data-fieldname=weight_curve]').html(`
						<p class="text-muted text-center">${empty_text}</p>
					`);
				}
			}
		}
	});
};

const add_folder_print_btn = (frm) => {
	frm.page.add_menu_item(__("Print complete record"), () => {
		print_complete_record(frm);
	}, false)
}

const printable_doctypes = ["Patient Record", "Pregnancy", "Gynecology", "Prenatal Interview", "Perineum Rehabilitation",
"Birth Preparation Consultation", "Early Postnatal Consultation", "Free Consultation", "Gynecological Consultation", "Perineum Rehabilitation Consultation",
"Postnatal Consultation", "Pregnancy Consultation", "Prenatal Interview Consultation"]

const print_complete_record = (frm) => {
	frappe.xcall("frappe.desk.form.linked_with.get", {doctype: frm.doctype, docname: frm.docname})
	.then((dt) => {
		let linked_doctypes = {}
		Object.keys(dt).forEach(value => {
			if (printable_doctypes.includes(value)) { linked_doctypes[value] = dt[value] }
		})
		return linked_doctypes
	})
	.then((docs) => {
		docs[frm.doctype] = [{"name": frm.doc.name}]
		print_record(frm, docs)
	})
}

const print_record = (frm, docs) => {
	if (Object.keys(docs).length > 0) {
		const dialog = new frappe.ui.Dialog({
			title: __('Print complete record'),
			fields: [{
				'fieldtype': 'Check',
				'label': __('With Letterhead'),
				'fieldname': 'with_letterhead',
				'default': 1
			},
			{
				'fieldtype': 'Check',
				'label': __('With Attachments'),
				'fieldname': 'with_attachments',
				'default': 0
			}]
		});

		dialog.set_primary_action(__('Print'), args => {
			if (!args) return;
			dialog.hide()
			frappe.show_alert({ message: __("Patient record in preparation"), indicator: 'green'})
			frappe.xcall("toobibpro.patients.doctype.patient_record.patient_record.download_patient_record", {docs: docs, record: frm.doc, args: args})
			.then(() => {
				frm.sidebar.reload_docinfo();
				frm.reload_doc();
				frappe.show_alert({ message: __("Patient record available in the sidebar"), indicator: 'green' })
			})
			.catch((e) =>
				{console.log(e)
				frappe.show_alert({ message: __("An issue occured while preparing the patient record. Please contact the support."), indicator: 'red' })}
			)
		});

		dialog.show();
	} else {
		frappe.msgprint(__('There must be at least 1 record to be printed'));
	}
}


Object.assign(toobibpro.patient_record, {
	set_dashboard_indicators: function(frm) {
		if(frm.doc.__onload && frm.doc.__onload.dashboard_info) {
			const info = frm.doc.__onload.dashboard_info
			frm.dashboard.add_indicator(__('Annual Revenue: {0}',
				[format_currency(info.billing_this_year, info.currency)]), 'blue');
			frm.dashboard.add_indicator(__('Outstanding Amount: {0}',
				[format_currency(info.total_unpaid, info.currency)]),
				info.total_unpaid ? 'orange' : 'green');
			frm.dashboard.add_indicator(__('Social Security Outstanding Amount: {0}',
				[format_currency(info.total_unpaid_social_security, info.currency)]),
				info.total_unpaid_social_security ? 'orange' : 'green');
		}
	}
})

class DocumentGrid {
	constructor(opts) {
		Object.assign(this, opts)
		this.prepare_wrapper()
		this.make_grid()
	}

	prepare_wrapper() {
		this.wrapper.html("")
		this.file_view = $(`<div class="file-view"></div>`).appendTo(this.wrapper)
		this.wrapper.addClass("file-grid-view")
	}

	make_grid() {
		Object.keys(this.data).map(dt => {
			Object.keys(this.data[dt]).map(dn => {
				const ref_wrapper = this.add_ref_to_grid(dt, dn)
				this.add_files_to_grid(this.data[dt][dn], ref_wrapper)
			})
		})
	}

	add_ref_to_grid(dt, dn) {
		const title = `${__(dt)}: ${dn}`
		return $(`<div class="file-grid-view mb-5">
			${frappe.utils.get_form_link(
				dt,
				dn,
				true,
				title
			)}
			<div class="file-grid">
			</div>
		</div>`).appendTo(this.file_view)
	}

	add_files_to_grid(data, wrapper) {
		const html = data.map(d => {
			d = this.prepare_datum(d)

			const icon_class = d.icon_class + "-large";
			let file_body_html =
				d._type == "image"
					? `<div class="file-image"><img src="${d.file_url}" alt="${d.file_name}"></div>`
					: frappe.utils.icon(icon_class, {
							width: "40px",
							height: "45px",
						});
			const name = escape(d.name);
			return `<a href="${d.file_url}" class="file-wrapper ellipsis" data-name="${name}" target="_blank">
					<div class="file-body">
						${file_body_html}
					</div>
					<div class="file-footer">
						<div class="file-title ellipsis">${d._title}</div>
						<div class="file-creation">${this.get_creation_date(d)}</div>
					</div>
				</a>`
		}).join("")


		wrapper.find(".file-grid").append($(html))
	}

	prepare_datum(d) {
		let icon_class = "";
		let type = "";
		if (d.is_folder) {
			icon_class = "folder-normal";
			type = "folder";
		} else if (frappe.utils.is_image_file(d.file_name)) {
			icon_class = "image";
			type = "image";
		} else {
			icon_class = "file";
			type = "file";
		}

		let title = d.file_name || d.file_url;
		title = title.slice(0, 60);
		d._title = title;
		d.icon_class = icon_class;
		d._type = type;

		d.subject_html = `
			${frappe.utils.icon(icon_class)}
			<span>${title}</span>
			${d.is_private ? '<i class="fa fa-lock fa-fw text-warning"></i>' : ""}
		`;
		return d;
	}

	get_creation_date(file) {
		const [date] = file.creation.split(" ");
		let created_on;
		if (date === frappe.datetime.now_date()) {
			created_on = comment_when(file.creation);
		} else {
			created_on = frappe.datetime.str_to_user(date);
		}
		return created_on;
	}
}