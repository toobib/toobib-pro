from dataclasses import dataclass
from functools import cached_property
from typing import TYPE_CHECKING, Any

import frappe
from frappe import _

if TYPE_CHECKING:
	from frappe.core.doctype.docfield.docfield import DocField

	from toobibpro.patients.doctype.patient_record_memo_fields.patient_record_memo_fields import (
		PatientRecordMemoFields as MemoField,
	)

	from .memo import MemoContext


# Base classes


def serialize_to_dict(obj):
	if isinstance(obj, dict):
		return {k: serialize_to_dict(v) for k, v in obj.items()}
	elif isinstance(obj, list):
		return [serialize_to_dict(v) for v in obj]
	elif hasattr(obj, "__dict__"):
		return serialize_to_dict(obj.__dict__)
	else:
		return obj


@dataclass
class MemoItem:
	def as_dict(self):
		return serialize_to_dict(self)


@dataclass
class MemoSource:
	context: "MemoContext"

	def to_item(self, raw_item: "MemoField") -> MemoItem:
		raise NotImplementedError


@dataclass
class MemoItemAppearance:
	# icon: str | None = None
	# color: str | None = None
	important: bool = False


# Items


@dataclass
class MemoItem_KeyValuePair(MemoItem):
	label: str
	value: Any
	# value_type = Literal["string", "number", "date", "datetime", "time", "boolean"] | None = None
	source: MemoSource | None = None
	appearance: MemoItemAppearance | None = None
	type: str = "kv"


@dataclass
class MemoItem_Table(MemoItem):
	label: str
	rows: list[dict[str, Any]]
	headers: list[dict[str, Any]]
	source: MemoSource | None = None
	type: str = "table"


@dataclass
class MemoItem_Section(MemoItem):
	label: str
	children: list[MemoItem]
	appearance: MemoItemAppearance | None = None
	type: str = "section"


# Fetch


class FetchUtils:
	@classmethod
	def get_doc(cls, doctype: str, name: str):
		try:
			return frappe.get_doc(doctype, name)
		except frappe.DoesNotExistError:
			frappe.clear_last_message()
			return None

	@classmethod
	def get_value(cls, doctype: str, name: str, fieldname: str):
		if doc := cls.get_doc(doctype, name):
			return doc.get(fieldname)

	@classmethod
	def get_values(cls, doctype: str, name: str, fields: list[str]):
		if doc := cls.get_doc(doctype, name):
			return {fieldname: doc.get(fieldname) for fieldname in fields}

	@classmethod
	def get_list(cls, doctype: str, filters=None, fields: list[str] = None, **kwargs):
		docnames = frappe.get_list(doctype, filters=filters or {}, pluck="name", **kwargs)
		return [cls.get_values(doctype, docname, fields or ["name"]) for docname in docnames]


# Sources


@dataclass
class MemoSource_Docfield(MemoSource):
	doctype: str
	filters: dict
	fieldname: str

	def format(self, value: Any, fieldtype: str) -> str:
		return frappe.format_value(value, fieldtype, currency=self.default_currency)

	def get_docfield(self) -> "DocField":
		if meta := frappe.get_meta(self.doctype):
			if self.fieldname in meta.default_fields:
				return frappe._dict({"fieldtype": "Read Only"})
			if docfield := meta.get_field(self.fieldname):
				return docfield

	@cached_property
	def default_currency(self):
		return frappe.db.get_single_value("ToobibPro Settings", "currency")

	def to_item(self, raw_item: "MemoField"):
		label = _(raw_item.label, None, self.doctype)
		doc = FetchUtils.get_doc(self.doctype, self.filters)
		if not doc:
			return None

		if not self.context.eval(raw_item.display_condition, {"doc": doc}, default_value=True):
			return None

		value = doc.get(self.fieldname)
		df = self.get_docfield() or frappe._dict({"fieldtype": "Read Only"})

		shown = bool(
			self.context.eval(raw_item.display_condition or df.depends_on, {"doc": doc}, default_value=True)
		)
		if not shown:
			return None

		writeable = not df.read_only and not df.fieldtype == "Read Only" and not df.hidden

		return MemoItem_KeyValuePair(
			label=label,
			value=value,
			source={
				"doctype": doc.doctype,
				"name": doc.name,
				"fieldname": self.fieldname,
				"readOnly": not writeable,
				"df": df.as_dict(),
			},
			appearance=MemoItemAppearance(
				important=bool(self.context.eval(raw_item.important_condition, {"doc": doc})),
			),
		)


@dataclass
class MemoSource_Table(MemoItem):
	doctype: str
	fieldname: str
	filters: dict
	columns: list[str]

	def get_row_meta(self):
		if meta := frappe.get_meta(self.doctype):
			if docfield := meta.get_field(self.fieldname):
				if row_doctype := docfield.options:
					return frappe.get_meta(row_doctype)
		return None

	def fetch_rows(self):
		row_meta = self.get_row_meta()

		if not row_meta:
			return "error"

		docs = frappe.get_list(self.doctype, filters=self.filters, fields=["name"], as_list=True)
		docs = [doc[0] for doc in docs]

		rows = FetchUtils.get_list(
			row_meta.name,
			filters={
				"parent": ["in", docs],
			},
			fields=self.columns + ["name", "parenttype", "parent"],
			order_by="parenttype,parent,idx",
			parent_doctype=self.doctype,
		)
		for row in rows:
			for fieldname, value in row.items():
				if fieldname in self.columns:
					row[fieldname] = {
						"@source": {
							"doctype": row_meta.name,
							"name": row["name"],
							"fieldname": fieldname,
							"fieldtype": row_meta.get_field(fieldname).fieldtype,
							"parenttype": row["parenttype"],
							"parent": row["parent"],
						},
						"@value": value,
					}
		return rows

	def get_headers(self):
		row_meta = self.get_row_meta()

		if not row_meta:
			return []

		return [
			{
				"label": _(df.label, None, self.doctype),
				"fieldname": df.fieldname,
				"fieldtype": df.fieldtype,
				"options": df.options,
			}
			for df in (row_meta.get_field(fieldname) for fieldname in self.columns)
		]

	def to_item(self, raw_item: "MemoField"):
		rows = self.fetch_rows()
		if rows == "error":
			return MemoSource_Error(error="Could not find row doctype").to_item(raw_item)
		if not rows:
			return None
		return MemoItem_Table(
			label=_(raw_item.label, None, self.doctype),
			rows=rows,
			headers=self.get_headers(),
			source={
				"doctype": self.doctype,
				"fieldname": self.fieldname,
				"filters": self.filters,
				"columns": self.columns,
			},
		)


@dataclass
class MemoSource_Error(MemoSource):
	error: str

	def to_item(self, raw_item: "MemoField"):
		return {
			"label": _(raw_item.label, None, self.doctype),
			"value": "Error: " + self.error,
			"type": "kv",
		}
