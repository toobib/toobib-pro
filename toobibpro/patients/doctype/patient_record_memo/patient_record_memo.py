# Copyright (c) 2023, Association Toobib and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.modules.utils import export_module_json


class PatientRecordMemo(Document):
	def validate(self):
		meta_dict = {}
		for row in self.fields:
			if not row.label and row.link_doctype:
				if row.link_doctype in meta_dict:
					meta = meta_dict[row.link_doctype]
				else:
					meta = frappe.get_meta(row.link_doctype)
					meta_dict[row.link_doctype] = meta

				field = meta.get_field(row.field)
				row.label = field.label

	def on_update(self):
		if (
			frappe.flags.in_patch
			or frappe.flags.in_install
			or frappe.flags.in_migrate
			or frappe.flags.in_import
			or frappe.flags.in_setup_wizard
		):
			return

		if frappe.conf.developer_mode:
			export_module_json(self, True, "patients")


def sync_patient_memo():
	from frappe.modules.import_file import import_file_by_path

	frappe.flags.in_import = True
	path = frappe.get_module_path(
		"patients", "patient_record_memo", "patient_record_memo", "patient_record_memo.json"
	)
	import_file_by_path(path, force=True)
	frappe.flags.in_import = False
