// Copyright (c) 2023, Association Toobib and contributors
// For license information, please see license.txt

frappe.ui.form.on("Patient Record Memo", {
	refresh(frm) {
		// TODO: Change the grid API to be able to set data in the auto-refresh field on refresh
	}
})

frappe.ui.form.on("Patient Record Memo Fields", {
	link_doctype(frm, cdt, cdn) {
		const row = locals[cdt][cdn]
		set_autocomplete(frm, row)
	},
});

const set_autocomplete = (frm, row) => {
	if (!row.link_doctype) {
		return
	}

	get_fields_for_doctype(row.link_doctype).then(fields => {
		const fieldnames = fields
			.filter(f => !f.hidden)
			.map(f => {
				return {
					value: f.fieldname,
					label: f.label
				}
			})

		frm.get_field("fields").grid.grid_rows[row.idx - 1].on_grid_fields_dict["field"].set_data(fieldnames)
	})
}


function get_fields_for_doctype(doctype) {
	return new Promise((resolve) => frappe.model.with_doctype(doctype, resolve)).then(() => {
		return frappe.meta.get_docfields(doctype).filter((df) => {
			return (
				!frappe.model.layout_fields.includes(df.fieldtype)
			);
		});
	});
}