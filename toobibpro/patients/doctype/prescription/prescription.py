# Copyright (c) 2017, DOKOS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc


class Prescription(Document):
	pass


@frappe.whitelist()
def make_prescription_from_consultation(source_name, target_doc=None):
	return _make_prescription(source_name, target_doc)


def _make_prescription(source_name, target_doc=None, ignore_permissions=False):
	if not frappe.flags.args:
		return {}

	def set_missing_values(source, target):
		target.reference_doctype = frappe.flags.args.get("doctype")
		target.reference_name = source.name

	doclist = get_mapped_doc(
		frappe.flags.args.get("doctype"),
		source_name,
		{
			frappe.flags.args.get("doctype"): {
				"doctype": "Prescription",
				"field_map": {"patient_record": "patient_record", "patient_name": "patient_name"},
			}
		},
		target_doc,
		set_missing_values,
		ignore_permissions=ignore_permissions,
	)

	return doclist
