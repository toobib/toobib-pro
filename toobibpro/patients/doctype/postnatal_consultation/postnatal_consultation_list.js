frappe.listview_settings["Postnatal Consultation"] = {
	add_fields: ["patient_name", "patient_record", "pregnancy_folder"],
	onload(listview) {
		listview.filter_area.add(
			listview.doctype,
			"practitioner",
			"=",
			frappe.boot.practitioner || ""
		);
	},
};
