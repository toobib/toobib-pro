# Copyright (c) 2017, DOKOS and contributors
# For license information, please see license.txt


import frappe
from frappe.model.document import Document


class Codification(Document):
	pass


@frappe.whitelist()
def disable_enable_codification(status, name):
	frappe.db.set_value("Codification", name, "disabled", status)
