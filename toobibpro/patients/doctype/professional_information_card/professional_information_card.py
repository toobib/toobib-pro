# Copyright (c) 2015, DOKOS and contributors
# For license information, please see license.txt


import frappe
from frappe import _
from frappe.model.document import Document


class ProfessionalInformationCard(Document):
	def before_insert(self):
		if not self.full_name:
			self.full_name = self.first_name + " " + self.last_name

	def validate(self):
		if not self.full_name:
			self.full_name = self.first_name + " " + self.last_name

		if not self.google_calendar:
			self.google_calendar_sync_by_default = 0

		if not self.health_professional:
			doc = frappe.new_doc("Health Professional")
			doc.first_name = self.first_name
			doc.last_name = self.last_name
			doc.insert()
			self.health_professional = doc.name

	@frappe.whitelist()
	def create_user(self):
		user = self.add_user()

		if user:
			self.user = user.name
			self.save()

	def add_user(self):
		if not self.email:
			frappe.throw(_("Please add a work email"))

		if frappe.db.exists("User", self.email):
			new_user = frappe.get_doc("User", self.email)
		else:
			try:
				new_user = frappe.get_doc(
					{
						"doctype": "User",
						"user_type": "System User",
						"first_name": self.first_name,
						"last_name": self.last_name,
						"email": self.email,
						"send_welcome_email": 1,
					}
				).insert(ignore_permissions=True)
				new_user.save()
			except Exception:
				frappe.log_error(frappe.get_traceback())

		self.user = new_user.name
		self.reload()

		return new_user

	@frappe.whitelist()
	def register_roles(self, selected_roles=None):

		if not selected_roles:
			return

		user = frappe.get_doc("User", self.email)
		user.roles = []

		for role in selected_roles:
			user.append("roles", {"role": role})

		user.save()
