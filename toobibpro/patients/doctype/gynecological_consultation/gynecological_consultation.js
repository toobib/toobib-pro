// Copyright (c) 2019, DOKOS and contributors
// For license information, please see license.txt

frappe.provide("toobibpro")

toobibpro.consultations_common.setup_consultations_controller()

frappe.ui.form.on("Gynecological Consultation", {
	onload(frm) {
		frm.fields_dict['gynecological_folder'].get_query = function(doc) {
			return {
				filters: {
					"patient_record": doc.patient_record
				}
			}
		}
	},

	weight(frm) {
		const weight = frm.doc.weight;
		frappe.db.get_value("Patient Record", {'name': frm.doc.patient_record}, 'height', (data) => {
			if (data.height) {
				const bmi = Math.round(frm.doc.weight / Math.pow(data.height, 2));
				frm.set_value("body_mass_index", bmi);
			}
		});
	}
})

extend_cscript(cur_frm.cscript, new toobibpro.BaseConsultationController({ frm: cur_frm }));
