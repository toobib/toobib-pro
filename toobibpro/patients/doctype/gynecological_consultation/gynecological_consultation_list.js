frappe.listview_settings["Gynecological Consultation"] = {
	add_fields: ["patient_name", "patient_record", "gynecological_folder"],
	onload(listview) {
		listview.filter_area.add(
			listview.doctype,
			"practitioner",
			"=",
			frappe.boot.practitioner || ""
		);
	},
};
