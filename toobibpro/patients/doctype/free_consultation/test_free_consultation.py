# Copyright (c) 2015, DOKOS and Contributors
# See license.txt


import unittest
import frappe
from frappe.tests.utils import FrappeTestCase

from toobibpro.patients.doctype.free_consultation.free_consultation import FreeConsultation

test_records = frappe.get_test_records("Free Consultation")


@unittest.skip("TODO: Réactiver le calcul des tarifs dans invoicing.py")
class TestFreeConsultation(FrappeTestCase):
	def test_create_and_submit_invoice(self):
		c1: FreeConsultation = create_free_consultation(
			third_party_payment=True,  # Payé par la Sécurité Sociale (100% maternité)
			codification="SF 15 (Test)",
		)

		self.assertEqual(c1.patient_name, "_Test Patient1")
		self.assertEqual(c1.codification[0].codification, "SF 15 (Test)")

		self.assertEqual(c1.patient_price, 0)
		self.assertAlmostEqual(c1.total_price, 46)

		# check if invoice creation is working
		c1.create_and_submit_invoice()

		# TODO


def create_free_consultation(
	third_party_payment=False, codification=None, without_codification=None
):
	consultation = frappe.get_doc(
		{
			"doctype": "Free Consultation",
			"patient_record": "_Test Patient1",
			"practitioner": "_Test Practitioner",
			"consultation_date": "2017-07-05",
			"lump_sum_travel_allowance": 1,
			"codification": [
				{
					"codification": codification,
				},
			],
			"customer": "_Test Patient1",
			"without_codification": without_codification,
			"third_party_payment": third_party_payment,
			"consultation_purpose": "Motif de consultation libre",
		}
	)
	consultation.insert()

	return consultation
