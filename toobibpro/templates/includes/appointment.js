// Copyright (c) 2019,DOKOS and Contributors
// See license.txt

frappe.provide('toobibpro.appointment');

frappe.ready(function() {
	new toobibpro.appointment.AppointmentSelector({
		parent: $('.page_content'),
	});
});