// import BaseTimeline from "frappe/public/js/frappe/form/footer/base_timeline";

frappe.provide("toobibpro")

class BaseTimeline {
	constructor(opts) {
		Object.assign(this, opts);
		this.make();
	}

	make() {
		this.timeline_wrapper = $(`<div class="timeline-container">`);
		this.wrapper = this.timeline_wrapper;
		this.timeline_items_wrapper = $(`<div class="centered-timeline">`);

		this.timeline_wrapper.append(this.timeline_items_wrapper);

		this.parent.replaceWith(this.timeline_wrapper);
		this.timeline_items = [];
	}

	refresh() {
		this.render_timeline_items();
	}

	add_action_button(label, action, icon = null, btn_class = null) {
		let icon_element = icon ? frappe.utils.icon(icon, "xs") : null;
		this.timeline_actions_wrapper.show();
		let action_btn = $(`<button class="btn btn-xs ${btn_class || "btn-default"} action-btn">
			${icon_element}
			${label}
		</button>`);
		action_btn.click(action);
		this.timeline_actions_wrapper.find(".action-buttons").append(action_btn);
		return action_btn;
	}

	render_timeline_items() {
		this.timeline_items_wrapper.empty();
		this.timeline_items = [];
		this.doc_info = (this.frm && this.frm.get_docinfo()) || {};
		let response = this.prepare_timeline_contents();
		if (response instanceof Promise) {
			response.then(() => {
				this.timeline_items.sort(
					(item1, item2) => new Date(item2.creation) - new Date(item1.creation)
				);
				this.timeline_items.forEach(this.add_timeline_item.bind(this));
			});
		} else {
			this.timeline_items.sort(
				(item1, item2) => new Date(item2.creation) - new Date(item1.creation)
			);
			this.timeline_items.forEach(this.add_timeline_item.bind(this));
		}
	}

	prepare_timeline_contents() {
		//
	}

	add_timeline_item(item, append_at_the_end = false) {
		let timeline_item = this.get_timeline_item(item);
		if (append_at_the_end) {
			this.timeline_items_wrapper.append(timeline_item);
		} else {
			this.timeline_items_wrapper.prepend(timeline_item);
		}
		return timeline_item;
	}

	add_timeline_items(items, append_at_the_end = false) {
		items.forEach((item) => this.add_timeline_item(item, append_at_the_end));
	}

	add_timeline_items_based_on_creation(items) {
		items.forEach((item) => {
			this.timeline_items_wrapper.find(".timeline-item").each((i, el) => {
				let creation = $(el).attr("data-timestamp");
				if (creation && new Date(creation) < new Date(item.creation)) {
					$(el).before(this.get_timeline_item(item));
					return false;
				}
			});
		});
	}

	get_timeline_item(item) {
		// item can have content*, creation*,
		// timeline_badge, icon, icon_size,
		// hide_timestamp, is_card
		const timeline_item = $(`<div class="timeline-item timeline-${item.side || 'right'}"><div class="timeline-blank"></div></div>`);

		if (item.name == "load-more") {
			timeline_item.append(
				`<div class="timeline-load-more">
					<button class="btn btn-default btn-sm btn-load-more">
						<span>${item.content}</span>
					</button>
				</div>`
			);
			timeline_item.find(".btn-load-more").on("click", async () => {
				let more_items = await this.get_more_communication_timeline_contents();
				timeline_item.remove();
				this.add_timeline_items_based_on_creation(more_items);
			});
			return timeline_item;
		}

		timeline_item.attr({
			"data-doctype": item.doctype,
			"data-name": item.name,
			"data-timestamp": item.creation,
		});

		timeline_item.append(
			`<div class="timeline-content ${item.is_card ? "frappe-card" : ""}">`
		);
		let timeline_content = timeline_item.find(".timeline-content");

		const timestamp = !item.hide_timestamp && !item.is_card ? `<p class="text-muted small">${comment_when(item.creation)}</p>` : ""
		const content = `<div>${item.content}${timestamp}</div>`

		timeline_content.append(content);

		if (item.id) {
			timeline_content.attr("id", item.id);
		}

		if (item.icon) {
			timeline_content.append(`
				<div class="timeline-badge" title='${item.title || frappe.utils.to_title_case(item.icon)}'>
					${frappe.utils.icon(item.icon, item.icon_size || "md")}
				</div>
			`);
		} else if (item.timeline_badge) {
			timeline_content.append(item.timeline_badge);
		} else {
			timeline_content.append(`<div class="timeline-dot">`);
		}

		return timeline_item;
	}
}

toobibpro.patient_record_timeline = class PatientRecordTimeline extends BaseTimeline {
	async make() {
		super.make()

		this.parent.removeClass("empty-section")
		this.timeline_wrapper.addClass("centered-timeline");

		await this.get_folders();
		await this.get_prescription();
		await this.get_consultations();
		this.render_timeline_items();
		this.section.removeClass("empty-section");
	}

	prepare_timeline_contents() {
		this.timeline_items.push(...this.timeline_consultations);
		this.timeline_items.push(...this.timeline_prescriptions);
		this.timeline_items.push(...this.timeline_folders)
	}

	async get_folders() {
		this.timeline_folders = await frappe.call({
			method: "toobibpro.patients.doctype.patient_record.patient_record.get_folders",
			args: {
				patient: this.frm.doc.name
			}
		}).then(folders => {
			return folders.message.map(folder => {
				return {
					icon: folder.icon,
					creation: folder.date,
					content: `<h5>${__(folder.doc_type)}<span>
						${frappe.utils.get_form_link(
							folder.doc_type,
							folder.name,
							true,
							frappe.utils.icon("unhide", "sm"),
						)}</span></h5>
					`,
					title: __(folder.doc_type),
					side: "left"
				}
			})
		})
	}

	async get_consultations() {
		this.timeline_consultations = await frappe.call({
			method: "toobibpro.patients.doctype.patient_record.patient_record.get_consultations",
			args: {
				patient: this.frm.doc.name
			}
		}).then(consultations => {
			return consultations.message.map(consultation => {
				const prescriptions = this.consultations_prescriptions.filter(f => f.reference_doctype == consultation.doc_type && f.reference_name == consultation.name)

				let content = `<h5>${__(consultation.doc_type)}
					<span>${frappe.utils.get_form_link(
						consultation.doc_type,
						consultation.name,
						true,
						frappe.utils.icon("unhide", "sm"),
					)}</span>
				</h5>`

				if (consultation.consultation_purpose){
					content += `<p>${consultation.consultation_purpose}</p>`
				}

					prescriptions.forEach(p => {
						content += `<p class="text-muted small">${p.prescription_type}<span>${frappe.utils.get_form_link(
							"Prescription",
							p.name,
							true,
							frappe.utils.icon("unhide", "sm"),
						)}</span></p>`
					})

				return {
					icon: consultation.icon,
					creation: consultation.consultation_date,
					content: content,
					title: __(consultation.doc_type),
					side: "right",
					doctype: consultation.doc_type,
					docname: consultation.name
				}
			})
		})
	}

	async get_prescription() {
		const prescriptions = await frappe.call({
			method: "toobibpro.patients.doctype.patient_record.patient_record.get_prescriptions",
			args: {
				patient: this.frm.doc.name
			}
		}).then(prescriptions => {
			return prescriptions.message.map(prescription => {
				const content = `<h5>${__(prescription.prescription_type)}
					<span>${frappe.utils.get_form_link(
						"Prescription",
						prescription.name,
						true,
						frappe.utils.icon("unhide", "sm"),
					)}</span></h5>`

				return {
					icon: prescription.icon,
					creation: prescription.consultation_date,
					content: content,
					title: __("Prescription"),
					side: "right",
					reference_doctype: prescription.reference_doctype,
					reference_name: prescription.reference_name,
					name: prescription.name,
					prescription_type: prescription.prescription_type
				}
			})
		})

		this.timeline_prescriptions = prescriptions.filter(f => !(f.reference_doctype && f.reference_name))
		this.consultations_prescriptions = prescriptions.filter(f => f.reference_doctype && f.reference_name)
	}
}

