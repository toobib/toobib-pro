import { createApp } from "vue";
import MemoVue from "./Memo.vue";
import { MiniBus } from "../common/MiniBus";

export class MemoBase {
	/**
	 * @protected
	 * @returns {Promise<object>}
	 */
	async fetch_data() {
		throw new Error("MemoBase: You have to implement fetch_data()");
	}

	/**
	 * @protected
	 * @returns {Promise<HTMLElement>}
	 */
	async prepare_dom() {
		throw new Error("MemoBase: You have to implement prepare_dom()");
	}

	/** @protected */
	async render() {
		const body = await this.prepare_dom();

		this.parent?.setAttribute("data-loading", "true");

		const data = await this.fetch_data();
		if (window.dev_server) {
			console.log("Memo data", data); // eslint-disable-line no-console
		}
		this.render_data(data, body);

		this.parent?.removeAttribute("data-loading");
	}

	/** @protected */
	async bind() {
		// Bind global events
		this.bus.listen("memo:source:updated", this.refresh.bind(this));
		this.bus.listen("memo:refresh", this.refresh.bind(this));
	}

	/** @private */
	render_data(data, element) {
		if (data?.type !== "memo") {
			element.innerHTML = "ERROR";
			console.error(data); // eslint-disable-line
			throw new Error("Memo: expected res.message.type to be 'memo', got: " + data?.type);
		}

		if (this.app) {
			this.app.unmount();
		}
		element.innerHTML = "";
		this.app = createApp(MemoVue, { item: data });
		window.SetVueGlobals?.(this.app);
		this.app.provide("bus", this.bus);
		this.app.mount(element);
	}

	// Public API
	async refresh() {
		this.render();
	}

	get bus() {
		if (!this._bus) {
			this._bus = new MiniBus();
		}
		return this._bus;
	}
}

export class MemoPanel extends MemoBase {
	constructor({ wrapper } = {}) {
		super();
		if (wrapper instanceof HTMLElement) {
			this.wrapper = wrapper;
		} else if (window.jQuery && wrapper instanceof window.jQuery) {
			this.wrapper = wrapper[0];
		} else {
			throw new Error("MemoPanel: wrapper must be an HTMLElement or a jQuery object");
		}
	}

	/** @protected */
	async prepare_dom() {
		if (this.parent) return this.body; // already prepared

		const randomId = "memo-panel-" + frappe.utils.get_random(10);

		/** @type {HTMLElement} */
		this.parent = $(`<div class="memo-panel">
			<div class="memo-panel__spacer"></div>
			<div class="memo-panel-buttons">
				<button class="memo-panel-button btn-reset" aria-controls="${randomId}">
					<span class="memo-panel-button__icon">${frappe.utils.icon("sidebar-collapse", "sm")}</span>
					<span class="memo-panel-button__label">${__("Memo")}</span>
				</button>
			</div>
			<div class="memo-root memo-panel__body" id="${randomId}"></div>
		</div>`).appendTo(this.wrapper)[0];

		this.toggle_button = this.parent.querySelector(".memo-panel-button");
		this.body = this.parent.querySelector(".memo-panel__body");

		this.bind();

		return this.body;
	}

	/** @protected */
	bind() {
		super.bind();

		// Bind button
		this.toggle_button.addEventListener("click", () => {
			if (this.is_visible()) {
				this.set_state("closed");
			} else {
				this.set_state("open");
			}
		});
		/* this.parent.addEventListener("mouseenter", () => {
			if (this.get_state() === "closed") {
				this.set_state("peek");
			}
		});
		this.parent.addEventListener("mouseleave", () => {
			if (this.get_state() === "peek") {
				this.set_state("closed");
			}
		});
		this.body.addEventListener("click", () => {
			// Stabiliser le panneau de mémo lors d'une interaction.
			if (this.get_state() === "peek") {
				this.set_state("open");
			}
		}); */
	}

	/** @param {"closed" | "empty" | "peek" | "open"} state */
	set_state(state) {
		// HATEOAS
		this.parent?.setAttribute("data-state", state);
		this.toggle_button?.setAttribute("aria-expanded", String(this.is_visible()));
	}

	get_state() {
		return this.parent?.getAttribute("data-state");
	}

	is_visible() {
		return ["peek", "open"].includes(this.get_state());
	}

	switch_wrapper(wrapper) {
		// On transfère le panneau de mémo vers un autre wrapper.
		this.parent?.remove(); // hypothèse: this.parent est enfant de this.wrapper
		this.wrapper = wrapper;
		this.wrapper.appendChild(this.parent);

		// this.wrapper = wrapper;
		// this.parent?.remove();
		// this.parent = null; // forcer le re-rendu
		this.render();
	}
}

/*
const me = this;

const content = this.data.map(d => {
	if (d.type == "section") {
		return $(`<div class="memo-section">
				<div class="memo-section-label">${d.label}</div>
			</div>`);
	} else if (d.important) {
		return $(`<div class="memo-item important">
			<div class="memo-label">${d.label}</div>
			<div class="memo-value bold">${d.value}</div>
		</div>`);
	} else if (d.table) {
		const rows = d.value.map(v => {
			return `
				<tr>${v.map(w => `<td>${w}</td>`).join("")}</tr>
			`;
		}).join("");
		return $(`<div class="memo-item">
			<div class="memo-label">${d.label}</div>
			<div class="memo-value bold">
				<table class="table">
				<tbody>${rows}</tbody>
				</table>
			</div>
		</div>`);
	} else {
		const memo_row = $(`<div class="memo-item row">
			<div class="col-xs-5 memo-label">${d.label}</div>
			<div class="col-xs-5 memo-value bold">
				${d.value}
			</div>
			<div class="col-xs-2 memo-close" data-fieldname="${d.fieldname}" data-doctype="${d.doc_type}">
				${frappe.utils.icon("close", "sm")}
			</div>

		</div>`);

		memo_row.on("click", ".memo-close", function (e) {
			frappe.call({
				method: "toobibpro.patients.doctype.patient_record.dashboard.patient_dashboard.exclude_fields",
				args: {
					patient_record: me.patient_record,
					fieldname: $(this).data("fieldname"),
					doc_type: $(this).data("doctype")
				}
			}).then(() => {
				me.refresh();
			});
		});
		return memo_row;
	}
});
const content_wrapper = $(`<div></div>`).appendTo(this.body);

content.forEach(c => content_wrapper.append(c));
*/