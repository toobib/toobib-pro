// @ts-check

/**
 * @typedef {Object} MedicationRow
 * @property {"Medication"} type
 *
 * https://www.hl7.org/fhir/medicationadministration.html
 * @property {string} medication_text
 * @property {string} medication_code
 *
 * disabled property {string} [dosage_text_override] - Si modifié manuellement, tous les champs suivants seront vidés, et seul ce texte fera foi
 *
 * https://www.hl7.org/fhir/valueset-approach-site-codes.html
 * @property {string} [dosage_site] - Localisation physique de l'administration du médicament
 *
 * https://www.hl7.org/fhir/valueset-route-codes.html
 * @property {string} [dosage_route] - Voie d'administration, "voie orale"
 *
 * https://www.hl7.org/fhir/valueset-administration-method-codes.html
 * @property {string} [dosage_method] - Méthode d'administration, "inhalation"
 * @property {string} [dosage_method_extra] - Texte libre, "pendant les repas"
 *
 * @property {number} dosage_dose - Quantité à chaque prise, 1
 * @property {string} dosage_dose_unit - "cachet"
 *
 * https://www.hl7.org/fhir/datatypes.html#Timing
 * 3 fois tous les deux jours pendant 4 semaines
 * @property {number} occurrence_frequency - Nombre de prises par répétition, 3
 * @property {number} occurrence_period - Nombre de répétitions, 2
 * @property {string} occurrence_period_unit - Période de répétition, "day"
 * @property {string} occurrence_text - Texte libre (?), indication supplémentaire, "matin midi et soir"
 *
 * https://www.hl7.org/fhir/careplan.html
 * @property {number} plan_duration - Durée du traitement, 4
 * @property {string} plan_duration_unit - Unité de la durée du traitement, "week"
 *
 * https://www.hl7.org/fhir/plandefinition.html
 * @property {string} [action_condition] - Texte libre, recommandations et conditions, "en cas de démangeaisons"
 */

/**
 * @typedef {Object} TextRow
 * @property {"Text"} type
 * @property {string} text
 */

/**
 * @typedef {MedicationRow | TextRow} Row
 */

/**
 * Fonction de pluralisation
 * @param {number} count
 * @param {string} unit
 * @returns {string}
 */
export function quantityAndUnit(count, unit) {
	if (count <= 1) {
		return `${count} ${unit}`;
	} else {
		return `${count} ${unit}s`;
	}
}

/**
 * Fonction de pluralisation pour une fréquence
 * @param {number} rate
 * @param {string} unit
 * @param {string} indic
 * @returns {string}
 */
export function rateToText(rate, unit, indic) {
	let out = "";

	if (rate === 1) {
		out += `${rate} ${unit}`;
	} else {
		out += unit.replace("fois ", "");
	}

	if (indic) {
		out += ` ${indic}`;
	}
	return out;
}

const fr = {
	hour: "heure",
	hours: "heures",
	day: "jour",
	days: "jours",
	week: "semaine",
	weeks: "semaines",
	month: "mois",
	months: "mois",
};

export function pl(/** @type {number} */ count, /** @type {string} */ unit) {
	if (count >= 2) {
		unit += "s";
	}

	let t = frappe._(unit, null, "Prescription");
	if (t === unit && unit.length > 0) {
		t = fr[unit] ?? t;
	}
	return t;
}
