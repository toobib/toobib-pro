import { createApp } from "vue";
import { createPinia } from "pinia";

import PrescriptionBuilder from "./Prescription.vue";
import { MiniBus } from "../common/MiniBus";

export function renderPrescriptionBuilder({ htmlElement, initialState, persistState } = {}) {
	if (htmlElement._toobibVueApp) {
		htmlElement._toobibVueApp.unmount();
	}

	const app = createApp(PrescriptionBuilder);
	const bus = new MiniBus();

	htmlElement.innerHTML = "";
	htmlElement._toobibVueApp = app;

	window.SetVueGlobals?.(app);
	app.use(createPinia());
	app.provide("bus", bus);
	app.provide("initialState", initialState);
	app.mount(htmlElement);

	bus.listen("save", (state) => {
		persistState(state);
	});
}
