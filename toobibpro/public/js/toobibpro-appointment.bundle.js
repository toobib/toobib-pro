import "frappe/public/js/controls.bundle.js";
import "frappe/public/js/frappe/model/create_new.js";
import "frappe/public/js/dialog.bundle.js";
import "frappe/public/js/lib/moment";
import "frappe/public/js/frappe/utils/datetime.js";
import "frappe/public/js/frappe/event_emitter.js";

import "./toobibpro/appointment_calendar/appointment_selector";
