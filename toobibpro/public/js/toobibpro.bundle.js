import "./templates/address_list.html";
import "./templates/contact_list.html";
import "./toobibpro/letter_composer";
import "./controllers/consultations";
import "./controllers/record_timeline";
import "./controllers/patient_memo";
import "./conf";
