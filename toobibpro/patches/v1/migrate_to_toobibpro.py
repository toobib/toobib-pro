import frappe


def execute():
	workspaces = [
		("Maia Home", "Home"),
		("Maia Website", "Website"),
		("Maia Accounting", "Accounting"),
	]

	for workspace in workspaces:
		frappe.rename_doc("Workspace", workspace[0], workspace[1], force=True)

	for consultation_type in ["Gynecological Consultation", "Pregnancy Consultation"]:
		for document in frappe.get_all(consultation_type):
			consultation = frappe.get_doc(consultation_type, document.name)
			drug_prescription = consultation.drug_prescription_table
			lab_prescription = consultation.lab_prescription_table
			echography_prescription = consultation.echography_prescription_table

			def get_prescription(consultation_type, consultation):
				prescription = frappe.new_doc("Prescription")
				prescription.patient_record = consultation.patient_record
				prescription.date = consultation.date
				prescription.reference_doctype = consultation_type
				prescription.reference_name = consultation.name
				return prescription

			if drug_prescription:
				prescription = get_prescription(consultation_type, consultation)
				prescription.prescription_type = "Drug Prescription"
				prescription.drug_prescription_table = drug_prescription
				prescription.insert()
				prescription.submit()

			if lab_prescription:
				prescription = get_prescription(consultation_type, consultation)
				prescription.prescription_type = "Laboratory Prescription"
				prescription.lab_prescription_table = lab_prescription
				prescription.insert()
				prescription.submit()

			if echography_prescription:
				prescription = get_prescription(consultation_type, consultation)
				prescription.prescription_type = "Echography Prescription"
				prescription.echography_prescription_table = echography_prescription
				prescription.insert()
				prescription.submit()
