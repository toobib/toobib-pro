app_name = "toobibpro"
app_title = "ToobibPro"
app_publisher = "Association Toobib"
app_description = "Free and Open-Source Health Information System"
app_email = "help@toobib.org"
app_license = "GPLv3"
app_logo_url = "/assets/toobibpro/images/logo_toobib.png"

fixtures = [
	# "Color",
	# {
	# 	"doctype": "Role",
	# 	"filters": {"name": ("in", ("Midwife", "Patient", "Midwife Substitute"))},
	# },
	# {"doctype": "Website Theme", "filters": {"name": "ToobibPro"}},
	# {"doctype": "Web Page", "filters": {"name": "demo"}},
	# "Insights Query",
	# "Insights Dashboard"
]

error_report_email = "help@toobib.org"

app_include_js = "toobibpro.bundle.js"
app_include_css = "toobibpro.bundle.css"
app_include_icons = "toobibpro/icons/healthicons.svg"

doctype_js = {
	"Google Calendar": "public/js/toobibpro/google_calendar.js",
	"Google Settings": "public/js/toobibpro/google_settings.js",
	"Address": "public/js/toobibpro/address.js",
	"Pregnancy Consultation": "public/js/controllers/print_settings.js",
	"Prescription": "public/js/controllers/print_settings.js",
	"Prenatal Interview Consultation": "public/js/controllers/print_settings.js",
	"Prenatal Interview": [
		"public/js/controllers/print_settings.js",
		"public/js/controllers/folders.js",
	],
	"Pregnancy": ["public/js/controllers/print_settings.js", "public/js/controllers/folders.js"],
	"Postnatal Consultation": "public/js/controllers/print_settings.js",
	"Perineum Rehabilitation Consultation": "public/js/controllers/print_settings.js",
	"Perineum Rehabilitation": [
		"public/js/controllers/print_settings.js",
		"public/js/controllers/folders.js",
	],
	"Patient Record": ["public/js/controllers/print_settings.js", "public/js/controllers/folders.js"],
	"Gynecology": ["public/js/controllers/print_settings.js", "public/js/controllers/folders.js"],
	"Gynecological Consultation": "public/js/controllers/print_settings.js",
	"Free Consultation": "public/js/controllers/print_settings.js",
	"Early Postnatal Consultation": "public/js/controllers/print_settings.js",
	"Birth Preparation Consultation": "public/js/controllers/print_settings.js",
	"Expense": "public/js/controllers/accounting.js",
	"Revenue": "public/js/controllers/accounting.js",
}


website_context = {
	"favicon": "/assets/toobibpro/favicon.png",
	"splash_image": "/assets/toobibpro/images/logo_toobib.png",
}

# Migration
# ----------
before_migrate = ["toobibpro.customizations.before_migration_hooks.before_migrate"]
after_migrate = [
	"toobibpro.patients.doctype.patient_record_memo.patient_record_memo.sync_patient_memo"
]

# after install
after_install = [
	"toobibpro.patients.doctype.patient_record_memo.patient_record_memo.sync_patient_memo"
]

setup_wizard_complete = "toobibpro.setup.setup_wizard.setup_wizard.setup_complete"

jinja_template_functions = "toobibpro.utilities.utils.custom_template_functions"
boot_session = "toobibpro.startup.boot.boot_session"

# welcome message title
login_mail_title = "Nous sommes heureux de vous compter parmi nous !"

# calendar
calendars = ["Appointment"]

fullcalendar_scheduler_licence_key = "GPL-My-Project-Is-Open-Source"

gcalendar_integrations = {
	"Appointment": {
		"pull_insert": "toobibpro.appointments.doctype.appointment.appointment.insert_event_to_calendar",
		"pull_update": "toobibpro.appointments.doctype.appointment.appointment.update_event_in_calendar",
		"pull_delete": "toobibpro.appointments.doctype.appointment.appointment.cancel_event_in_calendar",
	}
}


# default footer
default_mail_footer = """<div style="text-align: center;">
	<a href="https://toobib.org" target="_blank" style="color: #8d99a6;">
		Envoyé par ToobibPro
	</a>
</div>"""

website_route_rules = [
	{"from_route": "/my-appointments", "to_route": "Appointment"},
	{
		"from_route": "/my-appointments/<path:name>",
		"to_route": "appointment_details",
		"defaults": {
			"doctype": "Appointment",
			"parents": [{"label": "Mes Rendez-Vous", "route": "my-appointments"}],
		},
	},
	{"from_route": "/receipts", "to_route": "Revenue"},
	{
		"from_route": "/receipts/<path:name>",
		"to_route": "receipt",
		"defaults": {
			"doctype": "Revenue",
			"parents": [{"label": "Receipts", "route": "receipts"}],
		},
	},
]

standard_portal_menu_items = [
	{
		"title": "Prendre rendez-Vous",
		"route": "/appointment",
		"reference_doctype": "Appointment",
		"role": "Patient",
	},
	{
		"title": "Mes rendez-Vous",
		"route": "/my-appointments",
		"reference_doctype": "Appointment",
		"role": "Patient",
	},
	{
		"title": "Mes reçus",
		"route": "/receipts",
		"reference_doctype": "Revenue",
		"role": "Patient",
	},
]

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/toobibpro/css/toobibpro.css"
# app_include_js = "/assets/toobibpro/js/toobibpro.min.js"

# include js, css files in header of web template
# web_include_css = "/assets/toobibpro/css/toobibpro.css"
# web_include_js = "/assets/toobibpro/js/toobibpro.js"

# Home Pages
# ----------

# application home page (will override Website Settings)

# website user home page (by Role)
# role_home_page = {
# 	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "toobibpro.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "toobibpro.install.before_install"
# after_install = "toobibpro.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config
notification_config = "toobibpro.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
permission_query_conditions = {
	"Revenue": "toobibpro.accounting.doctype.revenue.revenue.get_permission_query_conditions",
	"Expense": "toobibpro.accounting.doctype.expense.expense.get_permission_query_conditions",
	"Miscellaneous Operation": "toobibpro.accounting.doctype.miscellaneous_operation.miscellaneous_operation.get_permission_query_conditions",
	"Payment": "toobibpro.accounting.doctype.payment.payment.get_permission_query_conditions",
	"General Ledger Entry": "toobibpro.accounting.doctype.general_ledger_entry.general_ledger_entry.get_permission_query_conditions",
}

has_permission = {
	"Revenue": "toobibpro.accounting.utils.has_accounting_permissions",
	"Expense": "toobibpro.accounting.utils.has_accounting_permissions",
	"Miscellaneous Operation": "toobibpro.accounting.utils.has_accounting_permissions",
	"Payment": "toobibpro.accounting.utils.has_accounting_permissions",
	"General Ledger Entry": "toobibpro.accounting.utils.has_accounting_permissions",
}

has_website_permission = {
	"Revenue": "toobibpro.controllers.website_list_for_contact.has_website_permission",
}

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"Website Settings": {"on_update": "toobibpro.customizations.doc_events.check_default_web_role"},
	"Appointment": {
		"after_insert": "toobibpro.appointments.doctype.appointment.appointment.insert_event_in_google_calendar",
		"on_update": "toobibpro.appointments.doctype.appointment.appointment.update_event_in_google_calendar",
		"on_cancel": "toobibpro.appointments.doctype.appointment.appointment.delete_event_in_google_calendar",
		"on_trash": "toobibpro.appointments.doctype.appointment.appointment.delete_event_in_google_calendar",
	},
}

# Scheduled Tasks
# ---------------

scheduler_events = {
	# 	"all": [
	# 		"toobibpro.tasks.all"
	# 	],
	"daily": [
		"toobibpro.accounting.doctype.asset.asset.post_depreciations",
		"toobibpro.tasks.update_patient_birthday",
		"toobibpro.accounting.utils.auto_create_fiscal_year",
	],
	"hourly": ["toobibpro.appointments.doctype.appointment.appointment.flush"]
	# 	"weekly": [
	# 		"toobibpro.tasks.weekly"
	# 	]
	# 	"monthly": [
	# 		"toobibpro.tasks.monthly"
	# 	]
}

# Testing
# -------

before_tests = "toobibpro.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "toobibpro.event.get_events"
# }

auto_cancel_exempted_doctypes = ["Revenue", "General Ledger Entry", "Payment"]

# export_python_type_annotations = True
